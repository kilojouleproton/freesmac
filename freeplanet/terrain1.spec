
[spec]

; Format and options of this spec file:
options = "+Freeciv-2.6-spec"

[info]

artists = "
    Insert credit here[SMAC]
"

[file]
gfx = "freeplanet/terrain1"

[grid_main]

x_top_left = 1
y_top_left = 1
dx = 96
dy = 48
pixel_border = 1

tiles = { "row", "column", "tag"

; terrain
 7,  0, "t.l0.inaccessible1"
 7,  0, "-"
 7,  0, ""
 0,  0, "t.l0.lowflatarid1"
 1,  0, "t.l0.lowflatrainy1"
 0,  2, "t.l0.lowrockyarid1"
 2,  2, "t.l0.lowrockyrainy1"
 0,  0, "t.l0.midflatarid1"
 1,  0, "t.l0.midflatrainy1"
 0,  2, "t.l0.midrockyarid1"
 2,  2, "t.l0.midrockyrainy1"
 0,  0, "t.l0.highflatarid1"
 1,  0, "t.l0.highflatrainy1"
 0,  2, "t.l0.highrockyarid1"
 2,  2, "t.l0.highrockyrainy1"
 0,  4, "t.l0.lowflataridfungus1"
 1,  4, "t.l0.lowflatrainyfungus1"
 0,  4, "t.l0.lowrockyaridfungus1"
 1,  4, "t.l0.lowrockyrainyfungus1"
 0,  4, "t.l0.midflataridfungus1"
 1,  4, "t.l0.midflatrainyfungus1"
 0,  4, "t.l0.midrockyaridfungus1"
 1,  4, "t.l0.midrockyrainyfungus1"
 0,  4, "t.l0.highflataridfungus1"
 1,  4, "t.l0.highflatrainyfungus1"
 0,  4, "t.l0.highrockyaridfungus1"
 1,  4, "t.l0.highrockyrainyfungus1"
 2,  4, "t.l0.lowflataridforest1"
 3,  4, "t.l0.lowflatrainyforest1"
 2,  4, "t.l0.lowrockyaridforest1"
 3,  4, "t.l0.lowrockyrainyforest1"
 2,  4, "t.l0.midflataridforest1"
 3,  4, "t.l0.midflatrainyforest1"
 2,  4, "t.l0.midrockyaridforest1"
 3,  4, "t.l0.midrockyrainyforest1"
 2,  4, "t.l0.highflataridforest1"
 3,  4, "t.l0.highflatrainyforest1"
 2,  4, "t.l0.highrockyaridforest1"
 3,  4, "t.l0.highrockyrainyforest1"
 2,  0, "t.l0.lake1"
 2,  0, "t.l0.coast1"
 2,  0, "t.l0.shelf1"
 3,  0, "t.l0.ocean1"
 4,  4, "t.l0.lakefungus1"
 4,  4, "t.l0.coastfungus1"
 4,  4, "t.l0.shelffungus1"
 5,  4, "t.l0.oceanfungus1"
 6,  4, "t.l0.lakekelp1"
 6,  4, "t.l0.coastkelp1"
 6,  4, "t.l0.shelfkelp1"
 7,  4, "t.l0.oceankelp1"

;add-ons
 0,  6, "tx.oil_mine"
 1,  6, "tx.irrigation"
 2,  6, "tx.farmland"
 3,  6, "tx.mine"
 4,  6, "tx.pollution"
 5,  6, "tx.village"
 6,  6, "tx.fallout"
 7,  6, "tx.oil_rig"

 15,  0, "t.dither_tile"
 15,  0, "tx.darkness"
 15,  2, "mask.tile"
 15,  2, "t.unknown1"
  7,  0, "t.blend.arctic" ;ice over neighbors
 15,  3, "t.blend.coast"
 15,  3, "t.blend.lake"
 15,  4, "user.attention"
 15,  5, "tx.fog"

;goto path sprites
 14,  7, "path.step"            ; turn boundary within path
 14,  8, "path.exhausted_mp"    ; tip of path, no MP left
 15,  7, "path.normal"          ; tip of path with MP remaining
 15,  8, "path.waypoint"
}
