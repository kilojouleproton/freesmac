[spec]

; Format and options of this spec file:
options = "+Freeciv-2.6-spec"

[info]

artists = "
    Insert credit here[SMAC]
"

[file]
gfx = "freeplanet/overlain"

[grid_main]

x_top_left = 1
y_top_left = 1
dx = 96
dy = 48
pixel_border = 1

tiles = { "row", "column", "tag"
 0,  0, "ts.nutrients1"
 2,  0, "ts.minerals1"
 4,  0, "ts.energy1"
 1,  2, "tx.irrigation1"
 2,  2, "tx.mine1"
 3,  2, "tx.mining_platform1"
 4,  2, "tx.pollution1"
 5,  2, "tx.supply_pod1"
 7,  2, "tx.sea_supply_pod1"
 1,  4, "tx.soil_enricher1"
 4,  4, "tx.fallout1"
 0,  6, "base.bunker1"
 1,  6, "base.airbase1"
 2,  6, "base.ruins1"
 4,  6, "base.sensor_array1"
}

