[spec]

; Format and options of this spec file:
options = "+Freeciv-2.6-spec"

[info]

artists = "
    Insert credit here[SMAC]
"

[file]
gfx = "freeplanet/bases"

[grid_main]

x_top_left = 1
y_top_left = 1
dx = 96
dy = 72
pixel_border = 1

tiles = { "row", "column", "tag"
;;[HH][GS]
; 0,  0, "base.airbase_mg"
; 0,  1, "tx.airbase_full"
;;[HH]
; 0,  3, "base.fortress_fg"
; 0,  4, "base.fortress_bg"
;;[HH][GS]
; 1,  0, "base.airstrip_mg"
;[El]
 1,  1, "base.buoy_mg"
;[VC]
 1,  2, "base.ruins_mg"
;;[HH][GS]
; 1,  3, "base.outpost_fg"
; 1,  4, "base.outpost_bg"
;[SMAC]
 0,  2, "cd.occupied",
      "city.gaian_occupied_0",
      "city.hive_occupied_0",
      "city.university_occupied_0",
      "city.morganite_occupied_0",
      "city.spartan_occupied_0",
      "city.believer_occupied_0",
      "city.peacekeeper_occupied_0"
;[HH]
 0,  5, "city.disorder"
;blank defaults
 1,  5, "cd.city",
      "cd.city_wall"
}
