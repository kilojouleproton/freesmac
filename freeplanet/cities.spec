;
; The names for city tiles are not free and must follow the following rules.
; The names consists of 'style name' + '_' + 'index'. The style name is as
; specified in cities.ruleset file and the index only defines the read order
; of the images. The definitions are read starting with index 0 till the first
; missing value The index is checked against the city bonus of effect
; EFT_CITY_IMAGE and the resulting image is used to draw the city on the tile.
;
; Obviously the first tile must be 'style_name'_city_0 and the sizes must be
; in ascending order. There must also be a 'style_name'_wall_0 tile used to
; draw the wall and an occupied tile to indicate a military units in a city.
; The maximum number of images is only limited by the maximum size of a city
; (currently MAX_CITY_SIZE = 255).
;

[spec]

; Format and options of this spec file:
options = "+Freeciv-2.6-spec"

[info]

artists = "
   Credit goes here[SMAC]
"

[file]
gfx = "freeplanet/cities"

[grid_main]

x_top_left = 1
y_top_left = 1
dx = 100
dy = 75
pixel_border = 1

tiles = { "row", "column", "tag"
; gaian, hive, university, morganite, spartan, believer, peacekeeper
;gaian
 0,  0, "city.gaian_city_0"
 0,  1, "city.gaian_city_1"
 0,  2, "city.gaian_city_2"
 0,  3, "city.gaian_city_3"
 0,  4, "city.gaian_wall_0"
 0,  5, "city.gaian_wall_1"
 0,  6, "city.gaian_wall_2"
 0,  7, "city.gaian_wall_3"
;hive
 1,  0, "city.hive_city_0"
 1,  1, "city.hive_city_1"
 1,  2, "city.hive_city_2"
 1,  3, "city.hive_city_3"
 1,  4, "city.hive_wall_0"
 1,  5, "city.hive_wall_1"
 1,  6, "city.hive_wall_2"
 1,  7, "city.hive_wall_3"
;university
 2,  0, "city.university_city_0"
 2,  1, "city.university_city_1"
 2,  2, "city.university_city_2"
 2,  3, "city.university_city_3"
 2,  4, "city.university_wall_0"
 2,  5, "city.university_wall_1"
 2,  6, "city.university_wall_2"
 2,  7, "city.university_wall_3"
;morganite
 3,  0, "city.morganite_city_0"
 3,  1, "city.morganite_city_1"
 3,  2, "city.morganite_city_2"
 3,  3, "city.morganite_city_3"
 3,  4, "city.morganite_wall_0"
 3,  5, "city.morganite_wall_1"
 3,  6, "city.morganite_wall_2"
 3,  7, "city.morganite_wall_3"
;spartan
 4,  0, "city.spartan_city_0"
 4,  1, "city.spartan_city_1"
 4,  2, "city.spartan_city_2"
 4,  3, "city.spartan_city_3"
 4,  4, "city.spartan_wall_0"
 4,  5, "city.spartan_wall_1"
 4,  6, "city.spartan_wall_2"
 4,  7, "city.spartan_wall_3"
;believer
 5,  0, "city.believer_city_0"
 5,  1, "city.believer_city_1"
 5,  2, "city.believer_city_2"
 5,  3, "city.believer_city_3"
 5,  4, "city.believer_wall_0"
 5,  5, "city.believer_wall_1"
 5,  6, "city.believer_wall_2"
 5,  7, "city.believer_wall_3"
;peacekeeper
 6,  0, "city.peacekeeper_city_0"
 6,  1, "city.peacekeeper_city_1"
 6,  2, "city.peacekeeper_city_2"
 6,  3, "city.peacekeeper_city_3"
 6,  4, "city.peacekeeper_wall_0"
 6,  5, "city.peacekeeper_wall_1"
 6,  6, "city.peacekeeper_wall_2"
 6,  7, "city.peacekeeper_wall_3"

}
