[spec]

; Format and options of this spec file:
options = "+Freeciv-2.6-spec"

[info]

artists = "
    Insert credit here[SMAC]
"

[file]
gfx = "freeplanet/roads"

[grid_main]

x_top_left = 1
y_top_left = 1
dx = 96
dy = 48
pixel_border = 1

tiles = { "row", "column", "tag"
;roads
 0, 0, "road.road_isolated"
 0, 1, "road.road_n"
 0, 2, "road.road_ne"
 0, 3, "road.road_e"
 0, 4, "road.road_se"
 0, 5, "road.road_s"
 0, 6, "road.road_sw"
 0, 7, "road.road_w"
 0, 8, "road.road_nw"

;rails
 2, 0, "road.rail_isolated"
 2, 1, "road.rail_n"
 2, 2, "road.rail_ne"
 2, 3, "road.rail_e"
 2, 4, "road.rail_se"
 2, 5, "road.rail_s"
 2, 6, "road.rail_sw"
 2, 7, "road.rail_w"
 2, 8, "road.rail_nw"

;rivers
 4,  0, "road.river_s_n0e0s0w0"
 4,  1, "road.river_s_n0e0s0w1"
 4,  2, "road.river_s_n0e0s1w0"
 4,  3, "road.river_s_n0e0s1w1"
 4,  4, "road.river_s_n0e1s0w0"
 4,  5, "road.river_s_n0e1s0w1"
 4,  6, "road.river_s_n0e1s1w0"
 4,  7, "road.river_s_n0e1s1w1"
 6,  0, "road.river_s_n1e0s0w0"
 6,  1, "road.river_s_n1e0s0w1"
 6,  2, "road.river_s_n1e0s1w0"
 6,  3, "road.river_s_n1e0s1w1"
 6,  4, "road.river_s_n1e1s0w0"
 6,  5, "road.river_s_n1e1s0w1"
 6,  6, "road.river_s_n1e1s1w0"
 6,  7, "road.river_s_n1e1s1w1"

; River outlets

 4,  8, "road.river_outlet_n"
 4,  8, "road.river_outlet_e"
 4,  8, "road.river_outlet_s"
 4,  8, "road.river_outlet_w"

;forests
 8,  0, "t.l1.forest_n0e0s0w0"
 8,  1, "t.l1.forest_n1e0s0w0"
 8,  2, "t.l1.forest_n0e1s0w0"
 8,  3, "t.l1.forest_n1e1s0w0"
 8,  4, "t.l1.forest_n0e0s1w0"
 8,  5, "t.l1.forest_n1e0s1w0"
 8,  6, "t.l1.forest_n0e1s1w0"
 8,  7, "t.l1.forest_n1e1s1w0"
 10,  0, "t.l1.forest_n0e0s0w1"
 10,  1, "t.l1.forest_n1e0s0w1"
 10,  2, "t.l1.forest_n0e1s0w1"
 10,  3, "t.l1.forest_n1e1s0w1"
 10,  4, "t.l1.forest_n0e0s1w1"
 10,  5, "t.l1.forest_n1e0s1w1"
 10,  6, "t.l1.forest_n0e1s1w1"
 10,  7, "t.l1.forest_n1e1s1w1"

;land xenofungus
 0,  0, "t.l1.xenofungus_n0e0s0w0"
 0,  1, "t.l1.xenofungus_n1e0s0w0"
 0,  2, "t.l1.xenofungus_n0e1s0w0"
 0,  3, "t.l1.xenofungus_n1e1s0w0"
 0,  4, "t.l1.xenofungus_n0e0s1w0"
 0,  5, "t.l1.xenofungus_n1e0s1w0"
 0,  6, "t.l1.xenofungus_n0e1s1w0"
 0,  7, "t.l1.xenofungus_n1e1s1w0"
 2,  0, "t.l1.xenofungus_n0e0s0w1"
 2,  1, "t.l1.xenofungus_n1e0s0w1"
 2,  2, "t.l1.xenofungus_n0e1s0w1"
 2,  3, "t.l1.xenofungus_n1e1s0w1"
 2,  4, "t.l1.xenofungus_n0e0s1w1"
 2,  5, "t.l1.xenofungus_n1e0s1w1"
 2,  6, "t.l1.xenofungus_n0e1s1w1"
 2,  7, "t.l1.xenofungus_n1e1s1w1"

;sea xenofungus
 0,  0, "t.l1.xenofungus_n0e0s0w0"
 0,  1, "t.l1.xenofungus_n1e0s0w0"
 0,  2, "t.l1.xenofungus_n0e1s0w0"
 0,  3, "t.l1.xenofungus_n1e1s0w0"
 0,  4, "t.l1.xenofungus_n0e0s1w0"
 0,  5, "t.l1.xenofungus_n1e0s1w0"
 0,  6, "t.l1.xenofungus_n0e1s1w0"
 0,  7, "t.l1.xenofungus_n1e1s1w0"
 2,  0, "t.l1.xenofungus_n0e0s0w1"
 2,  1, "t.l1.xenofungus_n1e0s0w1"
 2,  2, "t.l1.xenofungus_n0e1s0w1"
 2,  3, "t.l1.xenofungus_n1e1s0w1"
 2,  4, "t.l1.xenofungus_n0e0s1w1"
 2,  5, "t.l1.xenofungus_n1e0s1w1"
 2,  6, "t.l1.xenofungus_n0e1s1w1"
 2,  7, "t.l1.xenofungus_n1e1s1w1"

;sea xenofungus
 12,  0, "t.l1.lowflataridfungus_n0e0s0w0"
 12,  1, "t.l1.lowflataridfungus_n1e0s0w0"
 12,  2, "t.l1.lowflataridfungus_n0e1s0w0"
 12,  3, "t.l1.lowflataridfungus_n1e1s0w0"
 12,  4, "t.l1.lowflataridfungus_n0e0s1w0"
 12,  5, "t.l1.lowflataridfungus_n1e0s1w0"
 12,  6, "t.l1.lowflataridfungus_n0e1s1w0"
 12,  7, "t.l1.lowflataridfungus_n1e1s1w0"
 14,  0, "t.l1.lowflataridfungus_n0e0s0w1"
 14,  1, "t.l1.lowflataridfungus_n1e0s0w1"
 14,  2, "t.l1.lowflataridfungus_n0e1s0w1"
 14,  3, "t.l1.lowflataridfungus_n1e1s0w1"
 14,  4, "t.l1.lowflataridfungus_n0e0s1w1"
 14,  5, "t.l1.lowflataridfungus_n1e0s1w1"
 14,  6, "t.l1.lowflataridfungus_n0e1s1w1"
 14,  7, "t.l1.lowflataridfungus_n1e1s1w1"

}
